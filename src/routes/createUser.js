const { createUsers } = require("../controller/createUsers");
const formData = require("multer")().array();
const router = require("express").Router();

router.post("/", formData, createUsers);

module.exports = router;
