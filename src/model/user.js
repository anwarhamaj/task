const { DataTypes, Model } = require("sequelize");
const sequelize = require("../util/mysql_database.js");

require("dotenv").config();

class User extends Model {}
User.init(
  {
    // Model attributes are defined here
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    userName: { type: DataTypes.STRING, unique: true, allowNull: false },
    Karma_score: { type: DataTypes.INTEGER, allowNull: false, defaultValue: 0 },
  },
  {
    // Other model options go here
    sequelize, // We need to pass the connection instance
    modelName: "users", // We need to choose the model name
    timestamps: true,
    indexes: [
      {
        name: "Karma_score_id",
        using: "BTREE",
        fields: [
          "id",
          {
            name: "Karma_score",
            order: "ASC",
          },
        ],
      },
    ],
  }
);

module.exports = User;
