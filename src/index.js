const sequelize = require("./util/mysql_database");
const app = require("./app");
require("dotenv").config();

const port = process.env.PORT || 8000;

const runServer = async () => {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
    sequelize
       //.sync({ force: true })
      //.sync({ alter: true })
      .sync()
      .then((result) => {
        app.listen(port, () => console.log(`Server is up at port: ${port}`));
      })
      .catch((err) => {
        console.log(err);
      });
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
};

runServer();
