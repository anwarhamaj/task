const sequelize = require("../util/mysql_database");
const { QueryTypes, Op } = require("sequelize");

exports.getUsers = async (req, res, next) => {
  const userId = req.params.userId;
  sequelize
    .query(
      "SELECT (SELECT MAX(`users`.`Karma_score`) FROM `users` )AS `maxScore`,(SELECT MIN(`users`.`Karma_score`) FROM `Users` AS `users` )AS `minScore`, `users`.`id`, `users`.`username`, `users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` AS `users` LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id` WHERE `users`.`id` =:userId",
      {
        replacements: { userId: userId },
        type: QueryTypes.SELECT,
      }
    )
    .then(async (user) => {
      // return res.send(user)
      if (user[0].Karma_score == user[0].maxScore) {
        const userHaveBiggerByKarma_score = await sequelize.query(
          "SELECT ROW_NUMBER() OVER (ORDER BY Karma_score DESC)+1 AS `position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` AS `users` LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id` WHERE `users`.`Karma_score` <= ':score' AND `users`.`id` != ':userId' ORDER BY `users`.`Karma_score` DESC LIMIT 4",
          {
            replacements: { userId: user[0].id, score: user[0].Karma_score },
            type: QueryTypes.SELECT,
          }
        );
        const resArray = [
          {
            position: 1,
            id: user[0].id,
            username: user[0].username,
            Karma_score: user[0].Karma_score,
            "image.id": user[0]["image.id"],
            "image.url": user[0]["image.url"],
          },
          ...userHaveBiggerByKarma_score,
        ];

        return res.render("table", { title: "User List", userData: resArray });
      } else if (user[0].Karma_score == user[0].minScore) {
        const userHaveSmallerByKarma_score = await sequelize.query(
          " SELECT ROW_NUMBER() OVER (ORDER BY Karma_score DESC) AS `position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id`WHERE `users`.`Karma_score` >= ':score' AND `users`.`id` != ':userId' ORDER BY `users`.`Karma_score` ASC, `position` DESC LIMIT 4",
          {
            replacements: { userId: user[0].id, score: user[0].Karma_score },
            type: QueryTypes.SELECT,
          }
        );
        var position = userHaveSmallerByKarma_score[0].position + 1;
        const resArray = [
          ...userHaveSmallerByKarma_score.reverse(),
          {
            position: position,
            id: user[0].id,
            username: user[0].username,
            Karma_score: user[0].Karma_score,
            "image.id": user[0]["image.id"],
            "image.url": user[0]["image.url"],
          },
        ];

        return res.render("table", { title: "User List", userData: resArray });
      } else {
        const userHaveBiggerByKarma_score = await sequelize.query(
          " SELECT ROW_NUMBER() OVER (ORDER BY Karma_score DESC) AS `position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url`FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id`WHERE `users`.`Karma_score` >= ':score' AND `users`.`id` != ':userId' ORDER BY `users`.`Karma_score` ASC , `position` DESC LIMIT 2",
          {
            replacements: { userId: user[0].id, score: user[0].Karma_score },
            type: QueryTypes.SELECT,
          }
        );
        const userHaveSmallerByKarma_score = await sequelize.query(
          " SELECT  `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url`FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id`WHERE `users`.`Karma_score` < ':score' AND `users`.`id` != ':userId' ORDER BY `users`.`Karma_score` DESC LIMIT 2",
          {
            replacements: { userId: user[0].id, score: user[0].Karma_score },
            type: QueryTypes.SELECT,
          }
        );
        var position =
          userHaveBiggerByKarma_score[userHaveBiggerByKarma_score.length - 1]
            .position;
        userHaveBiggerByKarma_score.map((user) => {
          user.position = position++;
        });
        position = userHaveBiggerByKarma_score[0].position;
        var specificUser = {
          position: --position,
          id: user[0].id,
          username: user[0].username,
          Karma_score: user[0].Karma_score,
          "image.id": user[0]["image.id"],
          "image.url": user[0]["image.url"],
        };
        userHaveSmallerByKarma_score.map((user) => {
          user.position = --position;
          // position--;
        });
        const resArray = [
          ...userHaveBiggerByKarma_score.reverse(),
          specificUser,
          ...userHaveSmallerByKarma_score,
        ];
        return res.render("table", { title: "User List", userData: resArray });
      }
    })
    .catch((err) => {
      return res.render("table", { title: "User List", userData: [] });
    });
};

exports.getUsersWithLimit = async (req, res, next) => {
  const userId = req.params.userId;
  const limit = req.params.limit - 1;
  sequelize
    .query(
      "SELECT (SELECT MAX(`users`.`Karma_score`) FROM `users` )AS `maxScore`,(SELECT MIN(`users`.`Karma_score`) FROM `Users` AS `users` )AS `minScore`, `users`.`id`, `users`.`username`, `users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` AS `users` LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id` WHERE `users`.`id` =:userId",
      {
        replacements: { userId: userId },
        type: QueryTypes.SELECT,
      }
    )
    .then(async (user) => {
      if (user[0].Karma_score == user[0].maxScore) {
        const userHaveBiggerByKarma_score = await sequelize.query(
          "SELECT ROW_NUMBER() OVER (ORDER BY Karma_score DESC)+1 AS `position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` AS `users` LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id` WHERE `users`.`Karma_score` <= ':score' AND `users`.`id` != ':userId' ORDER BY `users`.`Karma_score` DESC LIMIT :limit",
          {
            replacements: {
              userId: user[0].id,
              score: user[0].Karma_score,
              limit: limit - 1,
            },
            type: QueryTypes.SELECT,
          }
        );
        const resArray = [
          {
            position: 1,
            id: user[0].id,
            username: user[0].username,
            Karma_score: user[0].Karma_score,
            "image.id": user[0]["image.id"],
            "image.url": user[0]["image.url"],
          },
          ...userHaveBiggerByKarma_score,
        ];

        return res.render("table", { title: "User List", userData: resArray });
      } else if (user[0].Karma_score == user[0].minScore) {
        const userHaveSmallerByKarma_score = await sequelize.query(
          " SELECT ROW_NUMBER() OVER (ORDER BY Karma_score DESC) AS `position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url` FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id`WHERE `users`.`Karma_score` >= ':score' AND `users`.`id` != ':userId' ORDER BY `users`.`Karma_score` ASC, `position` DESC LIMIT :limit",
          {
            replacements: {
              userId: user[0].id,
              score: user[0].Karma_score,
              limit: limit - 1,
            },
            type: QueryTypes.SELECT,
          }
        );
        var position = userHaveSmallerByKarma_score[0].position + 1;
        const resArray = [
          ...userHaveSmallerByKarma_score.reverse(),
          {
            position: position,
            id: user[0].id,
            username: user[0].username,
            Karma_score: user[0].Karma_score,
            "image.id": user[0]["image.id"],
            "image.url": user[0]["image.url"],
          },
        ];

        return res.render("table", { title: "User List", userData: resArray });
      } else {
        if (limit === 0) {
          const bigUser = await sequelize.query(
            " SELECT ROW_NUMBER() OVER (ORDER BY Karma_score DESC) AS `position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url`FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id`WHERE `users`.`Karma_score` >= ':score' AND `users`.`id` != ':userId' ORDER BY `users`.`Karma_score` ASC , `position` DESC LIMIT 1",
            {
              replacements: {
                userId: user[0].id,
                score: user[0].Karma_score,
              },
              type: QueryTypes.SELECT,
            }
          );
          user[0].position = bigUser[0].position - 1;
          return res.render("table", { title: "User List", userData: user });
        } else {
          var limitForBigScore = 1;
          var limitForSmallScore = 1;
          if (limit % 2 == 0) {
            limitForBigScore = limit / 2;
            limitForSmallScore = limit / 2;
          } else {
            limitForBigScore = Math.ceil(limit / 2);
            limitForSmallScore = Math.floor(limit / 2);
          }
          const userHaveBiggerByKarma_score = await sequelize.query(
            " SELECT ROW_NUMBER() OVER (ORDER BY Karma_score DESC) AS `position`, `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url`FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id`WHERE `users`.`Karma_score` >= ':score' AND `users`.`id` != ':userId' ORDER BY `users`.`Karma_score` ASC , `position` DESC LIMIT :limit",
            {
              replacements: {
                userId: user[0].id,
                score: user[0].Karma_score,
                limit: limitForBigScore,
              },
              type: QueryTypes.SELECT,
            }
          );
          const userHaveSmallerByKarma_score = await sequelize.query(
            " SELECT  `users`.`id`, `users`.`username`,`users`.`Karma_score`, `image`.`id` AS `image.id`, `image`.`url` AS `image.url`FROM `users` AS `users`LEFT OUTER JOIN `images` AS `image` ON `users`.`imageId` = `image`.`id`WHERE `users`.`Karma_score` < ':score' AND `users`.`id` != ':userId' ORDER BY `users`.`Karma_score` DESC LIMIT :limit",
            {
              replacements: {
                userId: user[0].id,
                score: user[0].Karma_score,
                limit: limitForSmallScore,
              },
              type: QueryTypes.SELECT,
            }
          );
          var position =
            userHaveBiggerByKarma_score[userHaveBiggerByKarma_score.length - 1]
              .position;
          userHaveBiggerByKarma_score.map((user) => {
            user.position = position++;
          });
          position = userHaveBiggerByKarma_score[0].position;
          var specificUser = {
            position: --position,
            id: user[0].id,
            username: user[0].username,
            Karma_score: user[0].Karma_score,
            "image.id": user[0]["image.id"],
            "image.url": user[0]["image.url"],
          };
          userHaveSmallerByKarma_score.map((user) => {
            user.position = --position;
          });
          console.log(specificUser);
          const resArray = [
            ...userHaveBiggerByKarma_score.reverse(),
            specificUser,
            ...userHaveSmallerByKarma_score,
          ];
          return res.render("table", {
            title: "User List",
            userData: resArray,
          });
        }
      }
    })
    .catch((err) => {
      return res.render("table", {
        title: "User List",
        userData: [],
        message: err,
      });
    });
};
