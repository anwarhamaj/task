const Image = require("../model/image");
const User = require("../model/user");
exports.createUsers = (req, res, next) => {
  try {
    const numberOfRow = 100000;
    for (let index = 0; index < numberOfRow; index++) {
      var val = Math.floor(1000 + Math.random() * 9000);
      var username = "_" + Math.random().toString(36).slice(2, 9);
      Image.create(
        {
          url: "http://localhost:8000/image/imageName",
          user: {
            userName: username,
            Karma_score: val,
          },
        },
        {
          include: [Image.hasOne(User)],
        }
      ).catch((err) => {
        console.log(err);
      });
    }

    res.status(201).send({ message: "Add successful" });
  } catch (err) {
    res.status(500).send({ error: err });
  }
};
