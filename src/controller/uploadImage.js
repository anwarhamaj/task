const multer = require("multer");
const path = require("path");

const ImageStorage = multer.diskStorage({
  destination: "upload/images",
  file: (req, file, cb) => {
    cb(null, "upload/images");
  },
  filename: (req, file, cb) => {
    //console.log(file);
    cb(null, Date.now() + path.extname(file.originalname));
  },
});

const correctTypeImage = (req, file, cb) => {
  console.log(file.mimetype);
  if (
    file.mimetype == "image/png" ||
    file.mimetype == "image/jpg" ||
    file.mimetype == "image/jpeg" ||
    file.mimetype == "image/tif" ||
    file.mimetype == "image/bmp" ||
    file.mimetype == "image/webp" ||
    file.mimetype == "application/octet-stream"
  ) {
    return cb(null, true);
  } else {
    cb(null, false);

    return cb(new Error("please upload image/png/jpg/jpeg  "));
  }
};

exports.ImageUpload = multer({
  storage: ImageStorage,
  fileFilter: correctTypeImage,
});
